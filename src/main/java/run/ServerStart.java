package run;

import jetty.JettyServer;
import jndi.LDAPRefServer;
import org.apache.commons.cli.*;
import util.Colors;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ServerStart {
    public static final int jettyPort = 54846;
    public static final int ldapPort = 55521;
    private final JettyServer jettyServer;
    private final LDAPRefServer ldapRefServer;

    public ServerStart(URL codebase) {
        jettyServer = new JettyServer(jettyPort);
        ldapRefServer = new LDAPRefServer(ldapPort, codebase);
    }

    public static void main(String[] args) throws Exception {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(cmdlineOptions(), args);
        } catch (Exception e) {
            System.err.println("Cmdlines parse failed.");
            System.exit(1);
        }

        String address = cmd.getOptionValue('A');

        ServerStart servers = new ServerStart(new URL(String.format("http://%s:%d/", address, jettyPort)));
        System.out.println("[ADDRESS] >> " + address);
        System.out.println("[COMMAND] >> " + withColor("open calc", Colors.ANSI_BLUE));

        System.out.println("----------------------------Server Log----------------------------");
        System.out.println(getLocalTime() + " [JETTYSERVER]>> Listening on 0.0.0.0:" + jettyPort);

        Thread jettyThread = new Thread(servers.jettyServer);
        jettyThread.start();

        Thread ldapThread = new Thread(servers.ldapRefServer);
        ldapThread.start();
    }

    public static Options cmdlineOptions() {
        Options opts = new Options();

        Option addressOption = new Option("A", true, "The address of server(ip or domain).");
        addressOption.setRequired(true);

        opts.addOption(addressOption);

        return opts;
    }

    public static String getLocalTime() {
        Date d = new Date();
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(d);
    }

    public static Boolean isLinux() {
        return !System.getProperty("os.name").toLowerCase().startsWith("win");
    }

    public static String withColor(String str, String color) {
        return isLinux() ? color + str + Colors.ANSI_RESET : str;
    }
}
